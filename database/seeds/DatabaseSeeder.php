<?php

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class)->times(20)->create();
        $tags = factory(Tag::class)->times(50)->create();
        $posts = factory(Post::class)->times(100)->create();

        foreach ($posts as $post) {
            $tagsID = $tags->random(5)->pluck('id');
            $post->tags()->attach($tagsID);
        }
        // $this->call(UsersTableSeeder::class);
    }
}
